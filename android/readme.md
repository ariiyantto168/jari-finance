1. Jelaskan apa yang anda ketahui tentang Activity ?
Jawab :
activity juga digunakan untuk melakukan berbagai kegiatan yang diperlukan di dalam aplikasi tersebut seperti berpindah dari satu tampilan ke tampilan lainnya, menjalankan program dsb .


2. Jelaskan apa yang anda ketahui tentang Fragment ?
Jawab :
Fragment merupakan bagian dari sebuah activity yang mana sebuah fragment tidak akan ada bila tidak ada sebuah activity karena fragment membutuhkan akses dari activity untuk dapat dijalankan.

3. Sebuah Activity memiliki tahapan siklus hidup, di antaranya : onCreate(), onStart(), onStop(), dan onDestroy(). Jelaskan dengan singkat kapan masing-masing tahap siklus hidup diatas dipanggil. ?
Jawab :
siklus tersebut di jalankan di dalam fragment 
- Oncreate menginisialisasi komponen penting dari aktivitas untuk menampilkan tampilan user
Setelah onCreate() selesai, callback berikutnya selalu onStart().
- Saat onCreate() keluar, aktivitas memasuki status Dimulai, dan aktivitas menjadi terlihat oleh pengguna. Callback ini merupakan persiapan akhir aktivitas untuk dikirim ke latar depan dan menjadi interaktif.
- onResume()
Sistem memanggil callback ini sebelum aktivitas mulai berinteraksi dengan pengguna. Pada tahap ini, aktivitas berada di bagian atas tumpukan aktivitas, dan mencatat semua input pengguna. Sebagian besar fungsi inti aplikasi diimplementasikan dalam metode onResume().

Callback onPause() selalu mengikuti onResume().
- onPause()
Sistem memanggil onPause() saat aktivitas kehilangan fokus dan memasuki status Dijeda. Status ini terjadi ketika, misalnya, pengguna mengetuk tombol Kembali atau Terbaru. Saat sistem memanggil onPause() untuk aktivitas Anda, secara teknis berarti aktivitas Anda masih terlihat sebagian, tetapi yang paling sering adalah indikasi bahwa pengguna meninggalkan aktivitas, dan aktivitas tersebut akan segera memasuki status Berhenti atau Dilanjutkan.

Setelah onPause() selesai dijalankan, callback berikutnya adalah onStop() atau onResume(), bergantung pada apa yang terjadi setelah aktivitas memasuki status Dijeda.

- onStop()
Sistem memanggil onStop() saat aktivitas tidak lagi terlihat oleh pengguna. Hal ini dapat terjadi karena aktivitas diakhiri, aktivitas baru dimulai, atau aktivitas yang ada memasuki status Dilanjutkan dan menutupi aktivitas yang dihentikan. Dalam semua kasus ini, aktivitas yang dihentikan tidak lagi terlihat sama sekali.

Callback berikutnya yang dipanggil oleh sistem adalah onRestart(), jika aktivitas akan kembali berinteraksi dengan pengguna, atau oleh onDestroy() jika aktivitas ini benar-benar dihentikan.

- onRestart()
Sistem memanggil callback ini jika aktivitas dalam status Berhenti akan dimulai ulang. onRestart() memulihkan status aktivitas pada waktu ketika aktivitas dihentikan.

Callback ini selalu diikuti oleh onStart().

- onDestroy()
Sistem memanggil callback ini sebelum aktivitas diakhiri.

Callback ini adalah panggilan terakhir yang diterima aktivitas. onDestroy() biasanya diterapkan untuk memastikan bahwa semua resource aktivitas akan dilepas jika aktivitas atau proses yang berisi callback tersebut diakhiri.

4. Jelaskan bagaimana cara untuk menyimpan data seperti access token di Android ?
Jawab : 
Menyimpan token di mobile umumnya hampir sama dengan website dengan menggunakan call api dan di simpan di dalam storage token di apps aplikasi 
5. Jelaskan dengan singkat kapan terjadinya Application Not Responding (ANR) dan bagaimana kita dapat mencegah terjadi ANR ?
Jawab :
Cara pembuatan thread pekerja yang paling efektif untuk operasi berdurasi lebih lama adalah dengan class AsyncTask. Cukup luaskan AsyncTask dan implementasikan metode doInBackground() untuk menjalankan tugas tersebut. Untuk memposting perubahan progres kepada pengguna, Anda dapat memanggil publishProgress(), yang akan memanggil metode callback onProgressUpdate(). Dari implementasi onProgressUpdate()
6. Diketahui sebuah aplikasi dimana user dapat mengerjakan suatu pekerjaan secara offline. Selama pengerjaan secara offline, user tidak perlu menunggu pekerjaannya sudah tersinkron ke server atau belum. Ketika memiliki jaringan internet, maka pekerjaan yang dikerjakan secara offline akan disinkronkan secara otomatis ke server. 

Jelaskan apa yang dibutuhkan dan bagaimana mengatasi kasus di atas!
Jawab : dengan menyimpan cache di mobile aplikasi user yang tersimpan di mobile di saat memiliki jaringan internet fungsi yang telah di buat akan ter update di dalam server aplikasi 

7 . Diketahui sebuah aplikasi yang menggunakan library GSON sebagai JSON Serializer / Deserializer dengan konfigurasi buildTypes pada build.gradle (app) sebagai berikut : ?

Jawab : ada beberapa case seperti source yang besar bisa mengaktifkan manifly untuk memperkecil resource atau bisa Instant Run dan shrinkResources secara bersamaan dan beberapa konfigurasi step di gradle android studi