function fib(n) {
  const fibs = [0 ,1]
  
  for (let i = 2; i < n; i++) {
      fibs[i] = fibs[i - 1] + fibs[i - 2];
  }

  return fibs;
}

console.log(fib(5));