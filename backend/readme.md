1. Jelaskan apa itu Composer ?
Jawab :
composer adalah depedency manager yang fungsinya membutuhkan atau memerlukan library dari luar tidak hanya php yang memiliki depedency seperti Ruby yang menggunakan Gem ,nodejs npm dsb

2. Jelaskan apa yang anda ketahui tentang HTTP Middleware ?
Jawab :
sebuah blok kode yang dipanggil sebelum ataupun sesudah http request di proses sesuai kebutuhan penggunanya.

3. Jelaskan apa yang anda ketahui tentang Eloquent ?
Jawab :
fitur ini  untuk mengelola sebuah data yang ada pada database aplikasi yang digunakan seperti orm laravel/lumen , sequelize nodejs , gorm go dsb 

4. Diketahui database sebagai berikut : 

A. Buatlah query untuk mengambil daftar user yang meminjam buku "A" ?
Jawab :
SELECT Peminjaman.* FROM User, Buku
WHERE Buku.name=A;

B. Buatlah query untuk mengambil nama user yang meminjam buku "A" setidaknya 2 kali ?
Jawab:

SELECT COUNT(*)
FROM (SELECT user_id, book_id 
      FROM peminjaman)
WHERE user_id && book_id > 1 AND select from buku = "A";


C. Buatlah query untuk mengambil nama user yang belum pernah meminjam buku apapun ?
Jawab :
SELECT Peminjaman.user_id, FROM user_id a INNER JOIN User ON user.id
WHERE Peminjaman.user_id NOT IN (SELECT user_id FROM Peminjaman)